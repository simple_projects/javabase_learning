package com.wang.demo;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;

import java.util.Map;

public class GuavaDemo {
    public static void main(String[] args) {
        String input = "";
        boolean is = Strings.isNullOrEmpty(input);
        System.out.println(is);

        Joiner.MapJoiner joiner = Joiner.on(",").withKeyValueSeparator("=");
        Map<String, Object> map = Maps.newHashMap();
        map.put("wang", "haha");
        map.put("wang1", "haha");
        map.put("wang2", "haha");
        map.put("wang3", "haha");
        map.put("wang4", "haha");
        System.out.println(joiner.join(map));
    }
}
