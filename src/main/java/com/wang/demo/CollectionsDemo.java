package com.wang.demo;

import com.google.common.collect.Lists;

import java.util.*;

/**
 * <p>Description: 遍历Collections的用法</p>
 * <p>
 * <p>Copyright:  2018-2018 北京掌通未来科技有限公司.All rights reserved.</p>
 *
 * @author wangtonggui
 * @date 2018/7/25
 **/
public class CollectionsDemo {
    public static void main(String[] args) {
//        addAll();
//        asLifoQueue();
//        binarySearch();
        checkedCollection();
//        copy();
//        disjoint();
    }

    /**
     * 不知道干嘛用？？
     */
    private static void checkedCollection() {
        List<String> list1 = Lists.newArrayList("111");
        System.out.println(Collections.checkedCollection(list1, String.class));
    }

    /**
     * 返回true，如果两个集合没有相同元素
     */
    private static void disjoint() {
        List<String> list1 = Lists.newArrayList("11", "123");
        List<String> list2 = Lists.newArrayList("12", "34");
        System.out.println(Collections.disjoint(list1, list2));
    }

    /**
     * copy方法直接就否了吧，限制太多
     */
    private static void copy() {
        List<String> list1 = Lists.newArrayList();
        list1.add("11");
        list1.add("11");
        List<String> list2 = new ArrayList<>();
        list2.addAll(list1);
        System.out.println(list1.hashCode());
        System.out.println(list2.hashCode());
    }

    /**
     * 二分法查找，前提需要排序，查找效率极高，是indexOf的十倍以上，下面测试一下
     */
    private static void binarySearch() {
        List<String> list1 = new ArrayList<>();
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            list1.add(i + "");
        }
        System.out.println("制造序列需要的时间："+ (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        list1.indexOf("777777");
        System.out.println(String.format("indexOf，使用时间：%d", System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        Collections.binarySearch(list1, "77777");
        System.out.println(String.format("binarySearch 二分法，使用时间：%d", System.currentTimeMillis() - start));
    }

    private static void asLifoQueue() {
        // 必须是LinkedList
        LinkedList<String> list1 = new LinkedList<>();
        Queue<String> queue = Collections.asLifoQueue(list1);
        // 入栈
        queue.offer("11");
        System.out.println(String.format("offer入栈后，queue的长度是：%d", queue.size()));
        // 出栈
        queue.poll();
        System.out.println(String.format("poll出栈后，queue的长度是：%d", queue.size()));

        queue.offer("11");
        queue.offer("22");
        queue.offer("33");
        System.out.println(String.format("第一个元素 %s", queue.peek()));
        // peek方法和element方法差不多，不推荐使用
    }

    private static void addAll() {
        List<String> list1 = new ArrayList<>();
        list1.add("1");
        System.out.println(list1);
        Collections.addAll(list1, "2", "3");
        System.out.println(list1);
    }
}
